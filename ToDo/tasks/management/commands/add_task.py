from django.core.management.base import BaseCommand
from datetime import datetime
from django.contrib.auth.models import User


import requests


def get_token():
    data = {
        'username': 'username',
        'password': 'sonhouse1234'
    }

    url = 'http://127.0.0.1:8080/api-auth'
    response = requests.post(url=url, data=data)
    token = response.json().get('token', None)
    return token


class Command(BaseCommand):
    def handle(self, *args, **options):
        token = get_token()

        data = {
            'title': 'Implement JWT in Django...',
            'person': User.objects.last().pk,
            'due_to': datetime(month=12, year=2018, day=1)
        }

        url = 'http://127.0.0.1:8080/tasks/'
        headers = {'Authorization': 'JWT {}'.format(token)}
        response = requests.post(url=url, data=data, headers=headers)
        print(response.content)