from rest_framework import serializers

from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    person = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Task
        fields = ('id', 'title', 'due_to', 'person')
