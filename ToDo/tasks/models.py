from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    title = models.CharField(max_length=100)
    person = models.ForeignKey('auth.User', related_name='tasks', blank=True, on_delete=models.CASCADE)
    due_to = models.DateTimeField()

    def __str__(self):
        return 'Task: {}'.format(self.title)
